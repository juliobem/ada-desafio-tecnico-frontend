## Ada Desafio Front-end
Projeto realizado para o desafio técnico para a Ada, que consiste na criação de um quadro Kanban simples.

## Screenshot
![image.png](./image.png)

## Como instalar e rodar o projeto

### Acessar o aplicativo em desenvolvimento
1. Clonar o repositorio

- Primeiramente deve-se clonar o repositorio ```git clone [link do github]```

2. Instalar dependências

- Rode _yarn install_ ou _npm install_ em ambas as pastas _FRONT_ e _BACK_, esse comando irá instalar as dependências do projeto.

3. Rodar o server e client

- O Server pode ser rodado localmente acessando a pasta server(```cd BACK```) e rodando o comando ```yarn dev```

- O Front-end pode ser rodado localmente acessando a pasta client(```cd FRONT```) e rodando o comando ```yarn start```.

**Detalhes Relevantes:**
- No arquivo ```src/services/services.js``` a baseURL deve apontar para o endereço do servidor.
![image-1.png](./image-1.png)
- No arquivo ```src/services/api.js``` as credenciais utilizadas foram as apontadas na descrição do desafio.
![image-2.png](./image-2.png)

## Tecnologias Aplicadas

- Totalmente desenvolvido utilizando Javascript + React.
- Estilização utilizando styled-components, icones com react-icons e paleta de cores inspirada no logo da Ada.
- Foi utilizado o ESLint para análise do código e Prettier para formatação.
- DOMPurify e Marked para renderização do markdown no modo visualização.

import React from 'react';
import styled from 'styled-components';
import logo from '../../assets/ada-logo.png';

const HeaderContainer = styled.div`
  width: 100%;
  height: 100px;
  background-color: #000;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  align-content: center;
  box-shadow: 1px 1px 5px #a1f250;
`;

const Logo = styled.img`
  width: 100px;
`;

const Title = styled.h1`
  width: 100%;
  color: #a1f250;
`;

const Header = () => {
  return (
    <HeaderContainer>
      <Logo src={logo} />
      <Title>Ada Kanban | Desafio Técnico</Title>
    </HeaderContainer>
  );
};

export default Header;

import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import { getCards } from '../../services/services';

import Card from './Components/Card/Card';
import Column from './Components/Column/Column';
import CreateCard from './Components/CreateCard/CreateCard';

const BoardContainer = styled.div`
  margin-top: 50px;
  display: grid;
  grid-template-columns: 2fr 3fr 3fr 3fr;
  padding: 0 10px;
  height: 100%;
  min-height: 75vh;
`;

const Board = () => {
  const [cards, setCards] = useState([]);

  const fetchCards = async () => {
    const cards = await getCards();
    setCards(cards);
  };

  useEffect(() => {
    fetchCards();
  }, []);

  const filterCards = (column, obj) => {
    const filteredCards = obj.filter((e) => e.lista === column);

    return filteredCards;
  };

  return (
    <BoardContainer>
      <Column title="New">
        <CreateCard setCards={setCards} />
      </Column>
      <Column title="To Do">
        {cards &&
          filterCards('ToDo', cards).map((e) => {
            return (
              <Card
                setCards={setCards}
                title={e.titulo}
                description={e.conteudo}
                column={e.lista}
                id={e.id}
                cards={cards}
                key={e.id}
              />
            );
          })}
      </Column>
      <Column title="Doing">
        {cards &&
          filterCards('Doing', cards).map((e) => {
            return (
              <Card
                setCards={setCards}
                title={e.titulo}
                description={e.conteudo}
                column={e.lista}
                id={e.id}
                cards={cards}
                key={e.id}
              />
            );
          })}
      </Column>
      <Column title="Done">
        {cards &&
          filterCards('Done', cards).map((e) => {
            return (
              <Card
                setCards={setCards}
                title={e.titulo}
                description={e.conteudo}
                column={e.lista}
                id={e.id}
                cards={cards}
                key={e.id}
              />
            );
          })}
      </Column>
    </BoardContainer>
  );
};

export default Board;

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { addCard } from '../../../../services/services';
import { MdAddTask } from 'react-icons/md';

import {
  CardContainer,
  CardHeader,
  CardButton,
  CardDescription,
  CardButtonsContainer,
  InputText,
  InputTextArea
} from '../Card/styles';

const CreateCard = ({ setCards }) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const addBtnStyle = {
    display: 'flex',
    justifyContent: 'center'
  };

  const handleCreation = async (id) => {
    if (!title || !description) return;
    const res = await addCard(title, description);
    setCards((prevState) => [...prevState, res]);
    setTitle('');
    setDescription('');
  };

  return (
    <CardContainer>
      <CardHeader>
        <InputText type="text" onChange={(e) => setTitle(e.target.value)} value={title} />
      </CardHeader>
      <CardDescription>
        <InputTextArea
          type="text"
          onChange={(e) => setDescription(e.target.value)}
          value={description}
        />
      </CardDescription>
      <CardButtonsContainer style={addBtnStyle}>
        <CardButton onClick={() => handleCreation(title, description)}>
          <MdAddTask size={18} />
        </CardButton>
      </CardButtonsContainer>
    </CardContainer>
  );
};

CreateCard.propTypes = {
  setCards: PropTypes.func.isRequired
};

export default CreateCard;

import styled from 'styled-components';

const CardContainer = styled.div`
  width: 200px;
  height: 300px;
  margin: 10px;
  background-color: #000;
  border-radius: 5%;
  border: solid 1px #a1f250;
  box-shadow: 2px 2px 5px;
  color: #a1f250;
`;

const CardHeader = styled.div`
  display: flex;
  padding: 5px 10px;
  flex-direction: row;
  justify-content: space-between;
  border-bottom: 1px solid;
  height: 10%;
`;

const CardDescription = styled.div`
  height: 80%;
  padding: 10px;
  font-size: 14px;
  word-break: break-all;
  text-align: justify;
  overflow: auto;
`;

const CardButtonsContainer = styled.div`
  height: 10%;
  border-top: 1px solid;
  display: flex;
  padding: 5px 10px;
  flex-direction: row;
  justify-content: space-between;
`;

const CardButton = styled.span`
  cursor: pointer;
  &:hover {
    transform: scale(1.2);
    transition: all 0.1s;
  }
`;

const InputText = styled.input`
  background-color: #9da6a4;
  color: #000;
  width: 100%;
  height: 100%;
  padding: 2px;
`;

const InputTextArea = styled.textarea`
  width: 100%;
  height: 100%;
  background-color: #9da6a4;
  color: #000;
  padding: 2px;
  resize: none;
`;

const TitleText = styled.h4`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

export {
  InputTextArea,
  InputText,
  CardButton,
  CardButtonsContainer,
  CardHeader,
  CardDescription,
  CardContainer,
  TitleText
};

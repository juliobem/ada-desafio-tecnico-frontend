import React, { useState } from 'react';

import CardRegular from './CardRegular/CardRegular';
import CardEdit from './CardEdit/CardEdit';

const Card = ({ cards, title, description, column, id, setCards }) => {
  const [cardState, setCardState] = useState('REGULAR');

  const renderSwitch = (param) => {
    switch (param) {
      case 'EDIT':
        return (
          <CardEdit
            setCardState={setCardState}
            column={column}
            id={id}
            title={title}
            description={description}
            setCards={setCards}
            cards={cards}
          />
        );
      default:
        return (
          <CardRegular
            setCardState={setCardState}
            column={column}
            id={id}
            title={title}
            description={description}
            setCards={setCards}
            cards={cards}
          />
        );
    }
  };

  return renderSwitch(cardState);
};

export default Card;

import React from 'react';
import PropTypes from 'prop-types';

import { deleteCard, editCard } from '../../../../../services/services';
import { MdEdit, MdDeleteForever, MdChevronLeft, MdChevronRight } from 'react-icons/md';
import { marked } from 'marked';
import DOMPurify from 'dompurify';

import {
  CardContainer,
  CardHeader,
  CardButton,
  CardDescription,
  CardButtonsContainer,
  TitleText
} from '../styles';

const CardRegular = ({ title, description, setCardState, id, column, setCards, cards }) => {
  const handleDelete = async (id) => {
    const res = await deleteCard(id);
    setCards(res);
  };

  const handleMoveCardLeft = async () => {
    let newColumn;

    if (column === 'Doing') newColumn = 'ToDo';
    if (column === 'Done') newColumn = 'Doing';

    const editedCard = await editCard(id, title, description, newColumn);

    const cardIndex = cards.findIndex((e) => e.id === id);
    cards[cardIndex] = editedCard;
    setCards((prevState) => [...prevState, cards]);
  };

  const handleMoveCardRight = async () => {
    let newColumn;

    if (column === 'ToDo') newColumn = 'Doing';
    if (column === 'Doing') newColumn = 'Done';

    const editedCard = await editCard(id, title, description, newColumn);

    const cardIndex = cards.findIndex((e) => e.id === id);
    cards[cardIndex] = editedCard;
    setCards((prevState) => [...prevState, cards]);
  };

  return (
    <CardContainer>
      <CardHeader>
        <TitleText>{title}</TitleText>
        <CardButton onClick={() => setCardState('EDIT')}>
          <MdEdit size={18} />
        </CardButton>
      </CardHeader>
      <CardDescription
        dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(marked(description)) }}
      />
      <CardButtonsContainer>
        <CardButton onClick={() => handleMoveCardLeft()}>
          <MdChevronLeft size={20} />
        </CardButton>
        <CardButton onClick={() => handleDelete(id)}>
          <MdDeleteForever size={18} />
        </CardButton>
        <CardButton onClick={() => handleMoveCardRight()}>
          <MdChevronRight size={20} />
        </CardButton>
      </CardButtonsContainer>
    </CardContainer>
  );
};

CardRegular.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  column: PropTypes.string.isRequired,
  setCardState: PropTypes.func.isRequired,
  setCards: PropTypes.func.isRequired,
  cards: PropTypes.object.isRequired
};

export default CardRegular;

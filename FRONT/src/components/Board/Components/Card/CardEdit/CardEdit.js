import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { MdSave, MdCancel } from 'react-icons/md';
import { editCard } from '../../../../../services/services';

import {
  CardContainer,
  CardHeader,
  CardButton,
  CardDescription,
  CardButtonsContainer,
  InputText,
  InputTextArea
} from '../styles';

const CardEdit = ({ cards, id, title, description, column, setCardState, setCards }) => {
  const [newTitle, setNewTitle] = useState(title);
  const [newDescription, setNewDescription] = useState(description);

  const handleEditCard = async () => {
    const reqTitle = newTitle?.length > 0 ? newTitle : title;
    const reqDescription = newDescription?.length > 0 ? newDescription : description;

    const editedCard = await editCard(id, reqTitle, reqDescription, column);
    setCardState('REGULAR');

    const cardIndex = cards.findIndex((e) => e.id === id);
    cards[cardIndex] = editedCard;
    setCards((prevState) => [...prevState, cards]);
  };

  return (
    <CardContainer>
      <CardHeader>
        <InputText type="text" onChange={(e) => setNewTitle(e.target.value)} value={newTitle} />
      </CardHeader>
      <CardDescription>
        <InputTextArea
          type="text"
          onChange={(e) => setNewDescription(e.target.value)}
          value={newDescription}
        />
      </CardDescription>
      <CardButtonsContainer>
        <CardButton onClick={() => handleEditCard()}>
          <MdSave size={18} />
        </CardButton>
        <CardButton onClick={() => setCardState('REGULAR')}>
          <MdCancel size={18} />
        </CardButton>
      </CardButtonsContainer>
    </CardContainer>
  );
};

CardEdit.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  column: PropTypes.string.isRequired,
  setCardState: PropTypes.func.isRequired,
  setCards: PropTypes.func.isRequired,
  cards: PropTypes.object.isRequired
};

export default CardEdit;

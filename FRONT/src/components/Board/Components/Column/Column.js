import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
  width: 100%;
  max-height: 75vh;
  overflow-y: scroll;
  overflow-x: hidden;
  &:nth-child(2),
  &:nth-child(3) {
    border-left: 1px solid #a1f250;
    border-right: 1px solid #a1f250;
  }
`;

const ColumnTitleContainer = styled.div`
  text-align: center;
  color: #a1f250;
  &:after {
    display: block;
    clear: both;
    content: '';
    position: relative;
    left: 0;
    bottom: 0;
    height: 1px;
    width: 90%;
    border-bottom: 1px solid #a1f250;
    margin: 0 auto;
    padding: 4px 0px;
  }
`;

const ColumnContent = styled.div``;

const Column = ({ title = 'Titulo', children }) => {
  return (
    <ColumnContainer>
      <ColumnTitleContainer>
        <h2>{title}</h2>
      </ColumnTitleContainer>
      <ColumnContent>{children}</ColumnContent>
    </ColumnContainer>
  );
};

Column.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.any
};

export default Column;

import api from './api';

const loginInfo = {
  login: 'letscode',
  senha: 'lets@123'
};

let token = '';

export const login = async () => {
  try {
    const res = await api.post('login', loginInfo, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    });

    token = res?.data;
  } catch (error) {
    console.log('erro fazendo login', error);
  }
};

export const getCards = async () => {
  if (!token) await login();

  const res = await api
    .get('cards', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    .catch((error) => {
      console.log('error', error);
      if (error.response.status === 401) login();
    });

  return res?.data;
};

export const deleteCard = async (id) => {
  if (!token) await login();

  const res = await api
    .delete(`cards/${id}`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    .catch((error) => {
      console.log('error', error);
      if (error.response.status === 401) login();
    });

  return res?.data;
};

export const addCard = async (title, description) => {
  if (!token) await login();

  const res = await api
    .post(
      'cards',
      {
        titulo: title,
        conteudo: description,
        lista: 'ToDo'
      },
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      }
    )
    .catch((error) => {
      console.log('error', error);
      if (error.response.status === 401) login();
    });

  return res?.data;
};

export const editCard = async (id, title, description, column) => {
  if (!token) await login();

  const res = await api
    .put(
      `cards/${id}`,
      {
        id: id,
        titulo: title,
        conteudo: description,
        lista: column
      },
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      }
    )
    .catch((error) => {
      console.log('error', error);
      if (error.response.status === 401) login();
    });

  return res?.data;
};

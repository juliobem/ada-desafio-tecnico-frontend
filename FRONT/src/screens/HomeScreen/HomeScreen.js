import React from 'react';
import styled from 'styled-components';

import { Board, Header } from '../../components';

const PageContainer = styled.section`
  width: 100%;
  background-color: #1a2126;
  height: 100%;
  min-height: 100vh;
`;

const HomeScreen = () => {
  return (
    <PageContainer>
      <Header />
      <Board />
    </PageContainer>
  );
};

export default HomeScreen;
